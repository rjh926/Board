<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<html lang="ko">
<head>

    <title>소셜보드</title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx}/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" rel="stylesheet">
<script src="${ctx}/resources/js/jquery-2.1.3.js"></script>
<script src="${ctx}/resources/js/jquery.blockUI.js"></script>
</head>
<body>
<script type="text/javascript">

function confirm() {
	var user = document.getElementById('user').value;
	var frm = document.getElementById('frm');
	if(user==""){
		alert('로그인하세요');
		location.replace('${ctx}');
	}
	else if(document.getElementById('title').value == ""){
		alert('제목을 입력하세요');
		document.getElementById('title').focus();
	}
	else if(document.getElementById('textArea').value == ""){
		alert('내용을 입력하세요');
		document.getElementById('textArea').focus();
	}
	else if(document.getElementById('articleId').value==""){
		frm.setAttribute('action','${ctx}/article/regist.do');
		frm.submit();
	}
	else{
		frm.setAttribute('action','${ctx}/article/reply.do');
		frm.submit();
		
	}
	
}
</script>

<!-- Main Navigation ========================================================================================== -->
<%@include file="/WEB-INF/views/common/header.jsp" %>
<!-- Header ========================================================================================== -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">홈</a></li>
                    <li><a href="#">게시판</a></li>
                    <li class="active">${board.name } </li>
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->
<div class="container">
    <div class="row">
        
        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs">
                    <h4>게시판</h4>
                </div>
                <div> 
                <c:forEach items="${boardList}" var="b">
	                    <a href="${ctx}/article/list.do?id=${b.boardId}&pageNum=1"
	                       class="list-group-item hidden-xs">${b.name}</a>
                </c:forEach>
                </div>
            </div>
        </div>
        
        <div class="col-sm-9 col-lg-9">
            <div>
                <h3>게시글 등록</h3>
            </div>
          
            <div class="table-responsive">
                <div class="well">
                    <form action="" id ="frm" class="bs-example form-horizontal" method="POST">
                    <input type="hidden" name="boardId" value="${board.boardId }">
                    <input type="hidden" name="user" id ="user" value="${user.userId}">
                    <input type="hidden" id="articleId" name="articleId" value="${article.articleId}">
                
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">제목</label>

                                <div class="col-lg-10">
                                    <input type="text" id="title" name="title" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">내용</label>

                                <div class="col-lg-10">
                                    <textarea class="form-control" name="contents" rows="3" id="textArea"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="button" class="btn btn-primary" onclick="confirm();">확인</button>
                                    <button type="reset" class="btn btn-default">취소</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!-- Footer ========================================================================================== -->
<%@include file="/WEB-INF/views/layout/footer.jsp" %>
</div>

</body>
</html>