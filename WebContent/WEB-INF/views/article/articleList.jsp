<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9.
-->
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx}/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="${ctx}/resources/js/jquery.blockUI.js"></script>
    <title>소셜보드</title>

</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	var p= ${board.pageCount};
	$("#search").keyup(function(){
		var str = $(this).val();
		var boardId = ${board.boardId};
	
			$.ajax({
				type : 'post',
				url : '${ctx}/article/search.do',
				dataType : 'json',
				data :{
					"title" : str,
					"boardId" : boardId
				},
				success : function(result){
					var array = result.articles;
					$("#list").html("");
					
					$.each(array,function(index,item){
							$("#list").append('<tr>').trigger("create");
							$("#list").append('<td class="text-center">'+item.articleId+'</td>').trigger("create");
							$("#list").append('<td><a href="${ctx}/article/detail.do?articleId='+item.articleId+'&boardId='+item.boardID+'">'+item.title+' </a></td>').trigger("create")
							$("#list").append('<td class="text-center"> '+item.regDate+'</td>').trigger("create");
							$("#list").append('<td class="text-center">'+item.authotName+'</td>').trigger("create");
		                    $("#list").append('<td class="text-center">'+item.count+'</td>').trigger("create");
		                    $("#list").append('</tr>').trigger("create");		
					})
					
				},
				error : function(){
					alert('에러');
				}
			})
		
}).blur(function(){
	if($(this).val()==""){
		location.replace("${ctx}/article/list.do?id="+${board.boardId});
	}
	
});
	$("#pages").css('font-weight','bold');
	
	for(var i=0;i<p;i++)
	{
	$("#pages").append('<a href="http://localhost:8082/MyBoard/article/list.do?id=${board.boardId}&pageNum='+i+'">'+ (i+1) +' </a>')
	}
});
</script>

<!-- Main Navigation ================================================================================= -->
<%@include file="/WEB-INF/views/common/header.jsp" %>
<!-- Header ========================================================================================== -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">홈</a></li>
                    <li><a href="#">게시판</a></li>
                    <li class="active">${board.name } </li>
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->
<div class="container">
    <div class="row">
       
        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs">
                    <h4>게시판</h4>
                </div>
                <div>
                <c:forEach items="${boardList}" var="b">
	                    <a href="${ctx}/article/list.do?id=${b.boardId}&pageNum=1" class="list-group-item hidden-xs">${b.name}</a>
                </c:forEach>
                </div>

            </div>
        </div>
        
        <div class="col-sm-9 col-lg-9">
            <div>
                <h3>${board.name }</h3>
            </div>
            
            <div class="table-responsive">
            <form class="form-inline" method="post" action="${ctx}/article/search.do" style="align-content: center;">
            <input  type="text" id ="search" placeholder="제목으로 검색" style="position: absolute; right: 15px;top: 35px">
            </form>
                <table class="table table-striped table-bordered table-hover">
                    <colgroup>
                        <col width="100"/>
                        <col width="*"/>
                        <col width="120"/>
                        <col width="70"/>
                        <col width="50"/>
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="text-center">번호</th>
                        <th class="text-center">제목</th>
                        <th class="text-center">작성일</th>
                        <th class="text-center">작성자</th>
                        <th class="text-center">조회</th>
                    </tr>
                    </thead>
                    <tbody id = "list">
                    <c:forEach items="${board.articles}" var="article">
                    
			                        <tr>
			                            <td class="text-center">${article.articleId} </td>
			                            <td>
			                            <c:choose>
			                            <c:when test="${article.depth eq 0 }">
			                            	<a href="${ctx}/article/detail.do?articleId=${article.articleId}&boardId=${article.boardId}">
			                            	${article.title} </a>
			                            </c:when>
			                           <c:when test="${article.depth eq 1}">
			                            	<a href="${ctx}/article/detail.do?articleId=${article.articleId}&boardId=${article.boardId}">
			                            	&nbsp;ㄴRE :  ${article.title} </a>
			                            </c:when>
			                            <c:when test="${article.depth eq 2}">
			                            	<a href="${ctx}/article/detail.do?articleId=${article.articleId}&boardId=${article.boardId}">
			                            	&nbsp;&nbsp;&nbsp;ㄴ RE : ${article.title} </a>
			                            </c:when>
			                            </c:choose>
			                            </td>
			                            <td class="text-center">
			                             ${article.regDate}
			                            </td>
			                            <td class="text-center"><c:if test="${article.isDelete eq 'N' }">${article.authorName}</c:if> </td>
			                            <td class="text-center"><c:if test="${article.isDelete eq 'N' }">${article.count }</c:if></td>
			                        </tr>
			                      
			                        </c:forEach>
                    </tbody>
                </table>
                <span id="pages" ></span>
            </div>

            <div class="text-right">
                <a href="${ctx}/article/regist.do?boardId=${board.boardId}">
                	<button type="button" class="btn btn btn-warning">등록</button>
                </a>
            </div>
        </div>
    </div>

<!-- Footer ========================================================================================== -->
<%@include file="/WEB-INF/views/layout/footer.jsp" %>

</div>

</body>
</html>