<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9. 
-->
<!DOCTYPE html>
<c:set var ="ctx">${pageContext.request.contextPath}</c:set>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx }/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx }/resources/css/layout.css" rel="stylesheet">
<script src="${ctx }/resources/js/jquery-2.1.3.js"></script>
<script src="${ctx }/resources/js/jquery.blockUI.js"></script>
    <title>소셜보드</title>

</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#showList").click(function(){
		$("#boardlist").slideToggle("slow");
	})
})

</script>

<!-- Main Navigation ========================================================================================== -->
<%@include file="/WEB-INF/views/common/header.jsp" %>
<!-- Header ========================================================================================== -->

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="${ctx}/article/main.do">홈</a></li>
                    <li><a href="${ctx}/arcitcle/list.do">게시판</a></li>
                </ol>
            </div>
        </div>
    </div>


<!-- Container ======================================================================================= -->
<div class="container">
    <div class="row">
      
        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs" id="showList">
                    <h4>게시판</h4>
                </div>
                <div id="boardlist" style="display: none;">
                <c:forEach items="${boardList}" var="board">                 
                 <a href="${ctx}/article/list.do?id=${board.boardId}&pageNum=1" class="list-group-item hidden-xs">${board.name } </a>
                 </c:forEach>
                </div>

            </div>
        </div>
        
	        <div class="col-sm-9 col-lg-9">
	            <div>
	                <h5>게시판 관리에서 게시판을 등록해주세요.</h5>
	            </div>
	        </div>

    </div>

<!-- Footer ========================================================================================== -->
<%@include file="/WEB-INF/views/layout/footer.jsp" %>
</div>

</body>
</html>