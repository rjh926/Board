<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9.
-->
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath}</c:set>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx}/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" rel="stylesheet">
<script src="${ctx}/resources/js/jquery-2.1.3.js"></script>
<script src="${ctx}/resources/js/jquery.blockUI.js"></script>
    <title>소셜보드</title>
</head>
<body>

<!-- Main Navigation ========================================================================================== -->
<%@include file="/WEB-INF/views/common/header.jsp" %>
<!-- Header ========================================================================================== -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">홈</a></li>
                    <li><a href="#">게시판</a></li>
                    <li class="active">${board.name }</li>
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->


<div class="container">
    <div class="row">

        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs">
                    <h4>게시판</h4>
                </div>
                <div>
                <c:forEach items="${boardList}" var="b">
                    	<a href="${ctx}/article/list.do?id=${b.boardId}&pageNum=1" class="list-group-item hidden-xs">${b.name}</a>
	            </c:forEach>
                </div>
            </div>
        </div>

        <div class="col-sm-9 col-lg-9">
            <div>
                <h3>${board.name }</h3>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                   ${article.title } 
                </div>
                <div class="panel-body">
                    <div class="post">
                        
                         <strong>${article.authorName }</strong>
                         &nbsp;<span class="text-muted">${article.regDate}.</span>
                         &nbsp;<span class="text-muted">${article.count } 읽음</span>
                         
                         
                         
						<c:if test="${user.authority eq 'admin' || user.userId eq article.authorName && article.isDelete eq 'N'}">
                         <a href="${ctx}/article/modify.do?id=${article.articleId}" class="glyphicon glyphicon-cog pull-right" style="padding:10px">수정</a>
                         <a href="${ctx}/article/delete.do?id=${article.articleId}" class="glyphicon glyphicon-trash pull-right" style="padding:10px">삭제</a>
                   </c:if>
                  
                    </div>
                    <br>

                    <p style="padding:20px">
                        ${article.contents } 
                    </p>
                </div>
            </div>

            <div class="text-center">
            <c:if test="${article.depth ne 2}">
            <a href="${ctx}/article/reply.do?articleId=${article.articleId}&boardId=${article.boardId}">
            <button type="button" class="btn btn-success">답글쓰기</button>
            </a>
            </c:if>
                <a href="${ctx}/article/list.do?id=${article.boardId}">
                    <button type="button" class="btn btn-default">목록</button>
                </a>
            </div>
        </div>
    </div>

    <!-- Footer ========================================================================================== -->
<%@include file="/WEB-INF/views/layout/footer.jsp" %>

</div>

</body>
</html>