<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9.
-->
<!DOCTYPE html>
<c:set var = "ctx">${pageContext.request.contextPath }</c:set>
<html lang="ko">
<head>

    <title>소셜보드</title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx}/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" rel="stylesheet">
<script src="${ctx}/resources/js/jquery-2.1.3.js"></script>
<script src="${ctx}/resources/js/jquery.blockUI.js"></script>

</head>
<body>

<!-- Main Navigation ========================================================================================== -->
<!-- Header ========================================================================================== -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">홈</a></li>
                    <li><a href="${ctx}/article/main.do">게시판</a></li>
                    <li class="active">${board.name } </li>
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->

<div class="container">
    <div class="row">
       
        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs">
                    <h4>게시판</h4>
                </div>
                <div>
                <c:forEach items="${boardList}" var="b">
	            		<a href="${ctx}/article/list.do?id=${board.boardId}&pageNum=1" class="list-group-item hidden-xs">${b.name } </a>
                </c:forEach>
                </div>
            </div>
        </div>
     
        <div class="col-sm-9 col-lg-9">
            <div>
                <h3>${board.name} </h3>
            </div>

            <form action="${ctx}/article/modify.do" method="POST">
            <input type="hidden" name="articleId" value="${article.articleId}"> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	${article.title} 
                    </div>
                    <div class="panel-body">
                        <div class="post">
                            <div class="write_info">
                                <span class="name">${article.authorName} </span>
                                <span class="date"><span class="_val">${article.regDate} </span></span>

                            </div>

                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="contents"
                                      rows="5">${article.contents } </textarea>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary">저장</button>
                    <a href="#" class="btn btn-default">취소</a>
                </div>
            </form>
        </div>
    </div>

<!-- Footer ========================================================================================== -->
</div>

</body>
</html>