<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var ="ctx">${pageContext.request.contextPath}</c:set>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="">소셜보드</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li ><a href="${ctx }/article/main.do">홈</a></li>
                <c:if test="${user.authority eq 'admin'}">
                 <li ><a href="${ctx }/board/list.do">게시판 관리</a></li>
                </c:if>
               
            </ul>
            <ul class="nav navbar-nav navbar-right">
            	<c:choose>
            	<c:when test="${user eq null}">
            	
                <li><a href="${ctx}">로그인</a></li>
            	</c:when>
            	
            	<c:otherwise>
            	<li style="color: white; font-weight: bold;">${user.name}님 환영합니다!</li>
                <li><a href="${ctx}/common/logout.do">로그아웃</a></li>
            	</c:otherwise>
            	</c:choose>
                
            </ul>
        </div>
    </div>
</div>
