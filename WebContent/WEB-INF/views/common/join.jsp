<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var ="ctx">${pageContext.request.contextPath}</c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원가입</title>
<link href="${ctx}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx }/resources/css/style.css" rel="stylesheet">

<style type="text/css">
body {
	padding : 10% 10%;
}
</style>
</head>
<body>
<script src="${ctx}/resources/js/jquery-2.1.3.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#loginId").keyup(function(){
		if($(this).val().length>5){
			var id = $(this).val();
			
			$.ajax({
				
				type : 'POST',
				url: '${ctx}/common/idCheck.do',
				data : 
				{
					id : id
				},
				
				dataType: "text",
				success : function(result){
					
					if($.trim(result)=="ok"){
						$("#alert_id").html('사용가능한ID입니다.');
						$("#id_check").val('Y');
					}
					else{
						
						$("#alert_id").html('이미 가입된 ID입니다');
						$("#id_check").val('N');
						
					}
				},
				error : function(request,status,error){
					  alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			       }
			     


				
			})
		}
		else{
			$("#alert_id").html("5자이상입니다.");
		}
		
	});
	
	
	
});

function confirm(){
	var frm = document.getElementById('join');
	var id = document.getElementById('loginId');
	var pw = document.getElementById('password');
	var name = document.getElementById('name');
	
	if(id.value==""){
		alert('아이디를 입력하세요');
		id.focus();
	}
	else if(document.getElementById('id_check').value=="N"){
		alert('ID가 유효하지 않습니다');
		id.focus();
	}
	else if(pw.value==""){
		alert('비밀번호를 입력하세요');
		pw.focus();
	}
	else if(name.value==""){
		alert('이름을 입력하세요');
		name.focus();
	}
	else{
		frm.submit();
	}
}
</script>
	<h3>회원가입</h3>
	
	<br>
	<form id="join" action="${ctx}/common/join.do" method="post" >
	<input type="hidden" id="id_check" value="N">
		<table class="table">
			<tr>
				<th>ID</th>
				<td><input id="loginId"  name="loginId" class="form-control" type="text" value="" placeholder="ID를 입력해주세요.">
				<span id ="alert_id" style="color: red; font: bolder;"></span></td>
			</tr>
			<tr>
				<th>Password</th>
				<td><input id="password" name="password" class="form-control" type="password" value="" placeholder="비밀번호를 입력해주세요."></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><input id="name" name="name" class="form-control" type="text" value="" placeholder="이름을 입력해주세요."></td>
			</tr>
		</table><br>
		<div align="center"><input class="btn" type="button" value="취소" onclick="javascript:history.go(-1);"> <input class="btn btn-success" type="button" value="회원가입" onclick="confirm();"></div>
	</form>
	<br>
</body>
</html>
