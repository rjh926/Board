<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9.
-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<title>로그인Page</title>
    <style type="text/css">
        body {
            padding-top: 100px;
            padding-bottom: 40px;
            background-color: #ecf0f1;
        }
        .login-header {
            max-width: 400px;
            padding: 15px 29px 25px;
            margin: 0 auto;
            background-color: #2c3e50;
            color: white;
            text-align: center;
            -webkit-border-radius: 15px 15px 0px 0px;
            -moz-border-radius: 15px 15px 0px 0px;
            border-radius: 15px 15px 0px 0px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .login-footer {
            max-width: 400px;
            margin: 0 auto 20px;
            padding-left: 10px;
        }
        .form-signin {
            max-width: 400px;
            padding: 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            -webkit-border-radius: 0px 0px 15px 15px;
            -moz-border-radius: 0px 0px 15px 15px;
            border-radius: 0px 0px 15px 15px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 15px;
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
        .form-btn {
            text-align: center;
            padding-top: 20px;
        }

    </style>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
	<link href="resources/css/style.css" rel="stylesheet">
</head>
<body>

<script type="text/javascript">

window.onload = function() {
	document.getElementById('id').onkeyup = function() {
		document.getElementById('alert_id').innerHTML= "";
	}
	document.getElementById('password').onkeyup = function() {
		document.getElementById('alert_pw').innerHTML="";
	}	
}
function clickSubmit(){
	
	if(document.getElementById('id').value==""){
		document.getElementById('alert_id').innerHTML ="아이디를 입력하세요.";
		document.getElementById('id').focus();
	}
	else if(document.getElementById('password').value==""){
		document.getElementById('alert_pw').innerHTML="비밀번호를 입력하세요";
		document.getElementById('password').focus();
	}
	else 
	{	
		document.getElementById('frm').setAttribute('method','POST');
		document.getElementById('frm').submit();
	}
}


</script>
<%@include file="/WEB-INF/views/common/header.jsp" %>
<div class="container">

    <!-- header -->
    <div class="login-header">
        <h2 class="form-signin-heading">계층형 게시판</h2>
    </div>
  
  

    <!-- form -->
    <form id="frm" class="form-signin" action="${pageContext.request.contextPath}/common/login.do">
        <input id="id" type="text" class="form-control" name="inputEmail" placeholder="아이디(이메일)" required>
        <span id ="alert_id" style="color: red;font: bold;"></span>
        <input type="password"  id="password" class="form-control" name="inputPassword" placeholder="비밀번호" required>
        <span id ="alert_pw" style="color: red;font: bold;"></span>
        <div class="row form-btn">
            <button class="btn btn-large btn-warning" type="button" onclick="clickSubmit();">로그인</button>
            <a href="${pageContext.request.contextPath}/common/join.do" class="btn btn-large btn-default">회원가입</a>
        </div>
    </form>

    <!-- footer -->
    <div class="login-footer">
        <p>© NamooSori 2015.</p>
    </div>
</div>
</body>
</html>