<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 
 * COPYRIGHT (c) Nextree Consulting 2015
 * This software is the proprietary of Nextree Consulting.  
 * 
 * @author <a href="mailto:eschoi@nextree.co.kr">Choi, Eunsun</a>
 * @since 2015. 1. 9.
-->
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html lang="ko">
<head>
    <title>소셜보드</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${ctx}/resources/css/bootstrap_modify.css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" rel="stylesheet">
<script src="${ctx}/resources/js/jquery-2.1.3.js"></script>
<script src="${ctx}/resources/js/jquery.blockUI.js"></script>
</head>
<body>
<script type="text/javascript">
function confirm(){
	var frm = document.getElementById('frm');
	if(document.getElementById('name').value==""){
		alert('게시판 이름을 입력하세요');
		frm.focus();
	}
	else{
		frm.submit();
	}
}
</script>

<!-- Main Navigation ================================================================================= -->
<%@include file="/WEB-INF/views/common/header.jsp" %>
<!-- Header ========================================================================================== -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판 관리</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="${ctx}/board/list.do">게시판 관리</a></li>
                    <li><a>게시판명</a></li>
                    <li class="active">게시판 수정</li>
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->
<div class="container">
    <div class="row">
     
        <div style="z-index:1020" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group panel panel-success">
                <div class="panel-heading list-group-item text-center hidden-xs">
                    <h4>게시판 관리</h4>
                </div>
                <div>
                 <c:forEach items="${boardList}" var="b" >
                
                <a href="${ctx}/board/modify.do?id=${b.boardId}" class="list-group-item hidden-xs">${b.name} </a>
                
                </c:forEach>
			                    
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 text-left">
                            <a class="btn btn-link btn-sm btn-block" 
                               href="${ctx}/board/regist.do">신규게시판 개설</a>
                        </div>
                        <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 text-left">
                            <a class="btn btn-link btn-sm btn-block" href="#">전체 메일발송</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
        <div class="col-sm-9 col-lg-9">
            <div>
                <h3>게시판 수정</h3>
            </div>
           
            <div class="table-responsive">
                <div class="well">
                    <form action="${ctx}/board/modify.do" id="frm" class="form-horizontal" method="POST">
                    	<input type="hidden" name="boardId" value="${board.boardId }">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">게시판명</label>

                                <div class="col-lg-10">
                                    <input type="text" id="name" name="name" class="form-control" value="${board.name}">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="button" class="btn btn-primary" onclick="confirm();">확인</button>
                                    <button type="reset" class="btn btn-default">취소</button>
                                    <a href="${ctx }/board/delete.do?id=${board.boardId}" class="btn btn-danger">삭제</a>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

            </div>
        </div>
    </div>

<!-- Footer ========================================================================================== -->
<%@ include file="/WEB-INF/views/layout/footer.jsp" %>
</div>

</body>
</html>