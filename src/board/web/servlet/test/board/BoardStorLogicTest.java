package board.web.servlet.test.board;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import board.web.servlet.domain.Board;
import board.web.servlet.store.BoardStore;
import board.web.servlet.store.logic.BoardStoreLogic;

public class BoardStorLogicTest {
	
	BoardStore store;
	@Before
	public void init(){
		store = new BoardStoreLogic();
	}

	@Test
	public void testCreate() {
		
	}

	@Test
	public void testRetrieve() {
	//	fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		 //fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
	
	}
		

	@Test
	public void testRetrieveAll() {
		List<Board> boards = store.retrieveAll();
		assertEquals(2, boards.size());
	}

}
