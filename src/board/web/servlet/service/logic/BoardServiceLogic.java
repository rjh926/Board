package board.web.servlet.service.logic;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.Board;
import board.web.servlet.domain.Comment;
import board.web.servlet.domain.Paging;
import board.web.servlet.service.BoardService;
import board.web.servlet.store.ArticleStore;
import board.web.servlet.store.BoardStore;
import board.web.servlet.store.CommentStore;
import board.web.servlet.store.logic.ArticleStoreLogic;
import board.web.servlet.store.logic.BoardStoreLogic;
import board.web.servlet.store.logic.CommentStoreLogic;

public class BoardServiceLogic implements BoardService {

	BoardStore boards;
	ArticleStore articles;
	CommentStore comments;
	Paging paging;

	public BoardServiceLogic() {
		boards = new BoardStoreLogic();
		articles = new ArticleStoreLogic();
		comments = new CommentStoreLogic();
	}

	@Override
	public void registerArticle(Article article) {
		/*
		 * 
		 * depth ==1;
		 * 
		 * max : grpNo= #{grpNo} and depth =0,1인 레코드의 최댓값+1
		 * -----------------------------------------------------------------------------
		 * ----------- depth ==2 ; max : grpNo =#{grpNo} and depth= 2인 레코드의 최대 grpSeq;
		 * 만약 없을경우 grpSeq = article.grpSeq +1
		 * 
		 * grpSeq 밀기 : grp = #{grpNo} and depth depth 0,1,2 의 max 보다 큰 grpSeq +1 씩
		 */

		Date date = new Date();

		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		if (article.getDepth() == 0) { // 새글 생성
			article.setRegDate(sqlDate);
			article.setGrpNo(articles.maxGrpNo() + 1);
			article.setGrpSeq(0);
			articles.create(article);
		}

		else if (article.getDepth() == 1) { // 답글 생성
			int max = articles.maxGrpSeq(article.getGrpNo(), article.getDepth());
			article.setRegDate(sqlDate);
			article.setGrpSeq(max + 1);
			articles.create(article);
		}

		else if (article.getDepth() == 2) { // 답글의 답글 생성
			int max = articles.maxGrpSeq(article.getGrpNo(), article.getDepth());
			if (max == -1) {
				max = article.getGrpSeq();
			}
			System.out.println(max);
			HashMap<String, Integer> map = new HashMap<>();
			map.put("grpNo", article.getGrpNo());
			map.put("maxValue", max);
			articles.updateGrp(map);
			article.setGrpSeq(max + 1);
			article.setRegDate(sqlDate);
			articles.create(article);
			System.out.println(article.getGrpSeq());

		}

	}

	@Override
	public Article findArticle(String articleId) {
		Article result = articles.retrieve(articleId);

		if ("Y".equals(result.getIsDelete())) {
			result.setContents("삭제된 게시글입니다");
			result.setTitle("삭제된 게시글입니다.");

		} else {
			articles.update(result);
			List<Comment> list = comments.retrieveAll(result.getArticleId());
			result.setComments(list);
		}
		return result;
	}

	@Override
	public Board findBoard(String boardId, int pageNum) {
		Board result = boards.retrieve(boardId);
		List<Article> alist = articles.retrieveAll(result.getBoardId());
		List<Article> articles = null;
		for (Article a : alist) {
			if ("Y".equals(a.getIsDelete())) {
				a.setTitle("삭제된 데이터입니다");
			}
		}

		Paging page = new Paging(alist.size(), 10);
		int countPage = page.getPageCount();
		if (page.getPageCount() < pageNum) {
			page.setCurrentPage(page.getPageCount());
		} else {
			page.setCurrentPage(pageNum);
		}

		if (alist.size() <= 10) { // 전체 페이지 수가 10보다 작을때
			articles = alist;
		} 
		
		else {
			if (alist.size() / (pageNum * 10) == 0) {
				articles = alist.subList(page.getStartRow(), alist.size());
			} else {
				articles = alist.subList(page.getStartRow(), page.getEndRow());
			}
		}
		
		result.setArticles(articles);
		result.setPageCount(page.getPageCount());
		result.setPageCount(countPage);
		return result;
	}

	@Override
	public void removeArticle(String articleId) {
		articles.delete(articleId);

	}

	@Override
	public void modifyArticle(Article article) {
		articles.update(article);

	}

	@Override
	public void registerBoard(Board board) {
		boards.create(board);

	}

	@Override
	public List<Board> findAllBoards() {
		List<Board> result = boards.retrieveAll();
		return result;
	}

	@Override
	public void modifyBoard(Board board) {
		boards.update(board);

	}

	@Override
	public void removeBoard(String boardId) {
		boards.delete(boardId);
		List<Article> alist = articles.retrieveAll(boardId);

		for (Article a : alist) {
			articles.delete(a.getArticleId());
		}

	}

	@Override
	public void registerComment(Comment comment) {
		comments.create(comment);

	}

	@Override
	public void removeComment(String commentId) {
		comments.delete(commentId);

	}

	@Override
	public JSONObject selectByTitle(String title, String boardId) {
		JSONObject result = new JSONObject();
		JSONArray arr = new JSONArray();
		List<Article> article = articles.retrieveByTitle(title, boardId);
		for (Article a : article) {
			JSONObject obj = new JSONObject();
			obj.put("articleId", a.getArticleId());
			obj.put("regDate", a.getRegDate() + "");
			obj.put("title", a.getTitle());
			obj.put("contents", a.getContents());
			obj.put("authotName", a.getAuthorName());
			obj.put("boardID", a.getBoardId());
			obj.put("depth", a.getDepth());
			obj.put("count", a.getCount());
			arr.add(obj);
		}
		result.put("articles", arr);
		return result;
	}

}
