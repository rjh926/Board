package board.web.servlet.service.logic;

import java.util.List;

import org.json.simple.JSONObject;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.User;
import board.web.servlet.service.UserService;
import board.web.servlet.store.UserStore;
import board.web.servlet.store.logic.UserStoreLogic;

public class UserServiceLogic implements UserService {

	UserStore store;
	public UserServiceLogic() {
		store = new UserStoreLogic();
	}
	@Override
	public int login(String userId, String password) {
		User findUser = store.selectOne(userId);
		
		if(findUser == null) { // id 오류
			return 2;
		}
		else if(!password.equals(findUser.getPassword())) { // pw 오류
			return 3;
		}
		else if(findUser.getAuthority().equals("admin")) { // 관리자 로그인
			return 0;
		}
		else if(findUser.getAuthority().equals("user")) { // 사용자 로그인
			return 1;
		}
		else return 2;
	}

	@Override
	public void join(User user) {
		
		store.insert(user);

	}

	@Override
	public void remove(String userId, String password) {
		int check = this.login(userId, password);
		if(check == 1) store.delete(userId);

	}

	@Override
	public String checkId(String userId) {
		User user = store.selectOne(userId);
		if(user!=null)
		return user.getUserId();
		else return "";
	}
	@Override
	public User read(String userId) {
		User user = store.selectOne(userId);
		return user;
	}
	

}
