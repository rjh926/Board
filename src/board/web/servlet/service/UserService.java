package board.web.servlet.service;

import org.json.simple.JSONObject;

import board.web.servlet.domain.User;

public interface UserService {
	
	public int login(String userId, String password);
	public User read(String userId);
	public void join(User user);
	public void remove(String userId,String password);
	public String checkId(String userId);
	
	
	
	
	

}
