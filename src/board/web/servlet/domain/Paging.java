package board.web.servlet.domain;

public class Paging {

	private int totalCount;
	private int rowMax;
	private int pageCount;
	private int startRow;
	private int endRow;
	private int currentPage;
	
	public void setCurrentPage(int i){
		if(i > this.getPageCount())
			this.currentPage = this.getPageCount();
		else this.currentPage = i;
		
	}

	public Paging(int arraySize, int rowMax) {

		this.totalCount = arraySize;
		this.rowMax = rowMax;

	}

	public int getPageCount() {
		int pageCount = totalCount / rowMax;
		
		if (pageCount % rowMax > 1) {
			pageCount++;
		}

			return pageCount;
	}
	public int getStartRow(){
		int result = (currentPage-1)*rowMax;	
		return result;
	}
	public int getEndRow(){
		int result = this.getStartRow()+rowMax;
		
		return result;
	}

}
