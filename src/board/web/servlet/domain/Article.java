package board.web.servlet.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class Article implements Serializable {

    private static final long serialVersionUID = -4946269376442479040L;
    
    private String articleId;
    private String title;
    private String authorName;
    private Date regDate;
    private String contents;
    private int count;
    private List<Comment> comments;
    private String boardId;
    private int depth;
    private String isDelete;
    private String parentId;
    private List<Article> sonArticle;
    private int grpNo;
    private int grpSeq;
    
    
    
    public int getGrpNo() {
		return grpNo;
	}
	public void setGrpNo(int grpNo) {
		this.grpNo = grpNo;
	}
	public int getGrpSeq() {
		return grpSeq;
	}
	public void setGrpSeq(int grpSeq) {
		this.grpSeq = grpSeq;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public List<Article> getSonArticle() {
		return sonArticle;
	}
	public void setSonArticle(List<Article> sonArticle) {
		this.sonArticle = sonArticle;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
    
    public String getArticleId() {
        return articleId;
    }
    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public Date getRegDate() {
        return regDate;
    }
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
    public String getContents() {
        return contents;
    }
    public void setContents(String contents) {
        this.contents = contents;
    }
    public String getBoardId() {
        return boardId;
    }
    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "Article [articleId=" + articleId + ", title=" + title + ", authorName=" + authorName + ", regDate="
				+ regDate + ", contents=" + contents + "]";
	}
	
}
