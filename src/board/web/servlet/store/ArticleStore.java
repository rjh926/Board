package board.web.servlet.store;

import java.util.HashMap;
import java.util.List;

import board.web.servlet.domain.Article;

public interface ArticleStore {
    
    String create(Article article);
    List<Article> retrieveParent(String articleId);
    Article retrieve(String articleId);
    List<Article> retrieveAll(String boardId);
    List<Article> retrieveByTitle(String title,String boardId);
    void update(Article article);
    void delete(String articleId);
    void updateGrp(HashMap<String,Integer> map);
    int maxGrpSeq(int grpNo,int depth);
    int maxGrpNo();
    int articleCount(String boardId);
}
