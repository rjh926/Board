package board.web.servlet.store;

import java.util.List;

import board.web.servlet.domain.User;

public interface UserStore {
	
	public void insert(User user);
	public User selectOne(String userId);
	public void update(User user);
	public void delete(String userId);
	public List<User> selectAll();
	
}
