package board.web.servlet.store.logic;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class BoardSessionFactory {
	private static BoardSessionFactory instance;
	private static SqlSessionFactory factory ;
	private static final String RESOURCE = "mybatis-config.xml";
	
	private BoardSessionFactory() {	}
	
	public static BoardSessionFactory getInstance(){

		if(instance == null){
			instance = new BoardSessionFactory();
		}
		return instance;
				
	}
	
	public SqlSession getSession(){
		if(factory==null){
			Reader reader = null;
			
			try {
				reader = Resources.getResourceAsReader(RESOURCE);
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			factory = new SqlSessionFactoryBuilder().build(reader);
		}
		
		return factory.openSession();
	}

}
