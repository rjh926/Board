package board.web.servlet.store.logic;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import board.web.servlet.domain.Board;
import board.web.servlet.store.BoardStore;
import board.web.servlet.store.mapper.BoardMapper;

public class BoardStoreLogic implements BoardStore {
	
	BoardSessionFactory factory;
	public BoardStoreLogic() {
		factory = BoardSessionFactory.getInstance();
	}

	@Override
	public String create(Board board) {
		SqlSession session = factory.getSession();
		
		try{
			BoardMapper mapper = session.getMapper(BoardMapper.class);
			mapper.createBoard(board);
			
		}finally{
			session.commit();
			session.close();
			
			
		}
		return null;
	}

	@Override
	public Board retrieve(String boardId) {
		SqlSession session = factory.getSession();
		Board board  = null;
		try {
			BoardMapper mapper = session.getMapper(BoardMapper.class);
			board = mapper.selectOne(boardId);
			
		} finally {
			session.close();
		}
		return board;
	}

	@Override
	public void update(Board board) {
		SqlSession session = factory.getSession();
		try {
			BoardMapper mapper = session.getMapper(BoardMapper.class);
			mapper.update(board);
			
			
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void delete(String boardId) {
		SqlSession session = factory.getSession();
		
		try {
			BoardMapper mapper = session.getMapper(BoardMapper.class);
			mapper.delete(boardId);
			
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public List<Board> retrieveAll() {
		SqlSession session = factory.getSession();
		List<Board> boards = new ArrayList<Board>();
		
		try{
			BoardMapper mapper = session.getMapper(BoardMapper.class);
			boards = mapper.selectAll();
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			session.close();
			
		}
		
		return boards;
		}

}
