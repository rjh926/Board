package board.web.servlet.store.logic;

import java.util.ArrayList;
import java.util.List;
import org.apache.ibatis.session.SqlSession;

import board.web.servlet.domain.User;
import board.web.servlet.store.UserStore;
import board.web.servlet.store.mapper.UserMapper;

public class UserStoreLogic implements UserStore {

	BoardSessionFactory factory;

	public UserStoreLogic() {
		factory = BoardSessionFactory.getInstance();
	}

	@Override
	public void insert(User user) {
		SqlSession session = factory.getSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.create(user);
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public User selectOne(String userId) {
		User result = null;
		SqlSession session = factory.getSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.selectOne(userId);
		} finally {
			session.close();
		}
		return result;
	}

	@Override
	public void update(User user) {
		SqlSession session = factory.getSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.update(user);
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void delete(String userId) {
		SqlSession session = factory.getSession();

		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.delete(userId);
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public List<User> selectAll() {
		List<User> result = new ArrayList<User>();
		SqlSession session = factory.getSession();
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			result = mapper.selectAll();
		} finally {
			session.close();
		}
		return result;
	}

}
