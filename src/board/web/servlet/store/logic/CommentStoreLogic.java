package board.web.servlet.store.logic;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import board.web.servlet.domain.Comment;
import board.web.servlet.store.CommentStore;
import board.web.servlet.store.mapper.CommentMapper;

public class CommentStoreLogic implements CommentStore {

	BoardSessionFactory factory;
	
	public CommentStoreLogic() {
		factory = BoardSessionFactory.getInstance();
	}
	@Override
	public String create(Comment comment) {
		SqlSession session = factory.getSession();
		
		try {
			CommentMapper mapper = session.getMapper(CommentMapper.class);
			mapper.create(comment);
		} finally {
			session.commit();
			session.close();
		}
		return null;
	}

	@Override
	public List<Comment> retrieveAll(String articleId) {
		SqlSession session = factory.getSession();
		List<Comment> comments = null;
		
		try {
			CommentMapper mapper = session.getMapper(CommentMapper.class);
			comments = mapper.selectAll(articleId);
			
		} finally {
			session.close();
		}
		return comments;
	}

	@Override
	public void delete(String commentId) {
		SqlSession session = factory.getSession();
		try {
			CommentMapper mapper = session.getMapper(CommentMapper.class);
			mapper.delete(commentId);
		} finally {
			session.commit();
			session.close();
			// TODO: handle finally clause
		}

	}

}
