package board.web.servlet.store.logic;

import java.io.ObjectOutputStream.PutField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import board.web.servlet.domain.Article;
import board.web.servlet.store.ArticleStore;
import board.web.servlet.store.mapper.ArticleMapper;

public class ArticleStoreLogic implements ArticleStore {

	BoardSessionFactory factory;

	public ArticleStoreLogic() {
		factory = BoardSessionFactory.getInstance();
	}

	@Override
	public String create(Article article) {
		SqlSession session = factory.getSession();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			mapper.create(article);

		} finally {
			session.commit();
			session.close();
		}
		return null;
	}

	@Override
	public Article retrieve(String articleId) {
		SqlSession session = factory.getSession();
		Article article = null;

		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			article = mapper.selectOne(articleId);

		} finally {
			session.close();
		}
		return article;
	}

	@Override
	public List<Article> retrieveAll(String boardId) {
		SqlSession session = factory.getSession();
		List<Article> articles = null;
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			articles = mapper.selectAll(boardId);
		} finally {
			session.close();
		}
		return articles;
	}

	@Override
	public void update(Article article) {
		SqlSession session = factory.getSession();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			mapper.update(article);
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void delete(String articleId) {
		SqlSession session = factory.getSession();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			mapper.delete(articleId);

		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public List<Article> retrieveParent(String articleId) {
		SqlSession session = factory.getSession();
		List<Article> result = new ArrayList<Article>();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			result = mapper.selectByParent(articleId);

		} finally {
			session.close();
		}
		return result;
	}

	@Override
	public void updateGrp(HashMap<String, Integer> map) {
		SqlSession session = factory.getSession();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			mapper.updateSeq(map);

		} finally {
			session.commit();
			session.close();

		}

	}

	@Override
	public int maxGrpSeq(int grpNo, int depth) {
		SqlSession session = factory.getSession();
		HashMap<String, Integer> map = new HashMap<>();
		int max = 0;
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			
			map.put("grpNo", grpNo);
			map.put("depth", depth);
			if(mapper.maxGrpSeq(map)!=null) max = mapper.maxGrpSeq(map);
			else max = -1;
			
			
			
		}
		finally {
			session.close();
		}
			return max;
		
	}

	@Override
	public int maxGrpNo() {
		SqlSession session = factory.getSession();
		int max = 0;
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			try {
				max = mapper.maxGrpNo();
			} catch (Exception e) {
				max = 0;
			}

		} finally {
			session.close();
		}
		return max;
	}

	@Override
	public List<Article> retrieveByTitle(String title,String boardId) {
		
		SqlSession session = factory.getSession();
		List<Article> foundArticles = new ArrayList<>();
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			HashMap<String,String> hash = new HashMap<>();
			hash.put("title", title);
			hash.put("boardId",boardId);
			foundArticles = mapper.selectByTitle(hash);
			
		} finally {
			session.close();
		}
		return foundArticles;
	}

	@Override
	public int articleCount(String boardId) {
		SqlSession session = factory.getSession();
		int result = 0;
		try {
			ArticleMapper mapper = session.getMapper(ArticleMapper.class);
			result = mapper.articleCount(boardId);
			
		} finally {
			session.close();
		}
		return result;
	}

}
