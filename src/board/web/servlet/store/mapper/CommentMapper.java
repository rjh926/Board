package board.web.servlet.store.mapper;

import java.util.List;

import board.web.servlet.domain.Comment;

public interface CommentMapper {
	
	public void create(Comment comment);
	public List<Comment> selectAll(String articleId);
	public void delete(String commentId);

}
