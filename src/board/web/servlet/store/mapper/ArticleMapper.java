package board.web.servlet.store.mapper;

import java.util.HashMap;
import java.util.List;

import board.web.servlet.domain.Article;

public interface ArticleMapper {
	
	public void create(Article article);
	public Article selectOne(String articleId);
	public List<Article> selectByParent(String articleId);
	public List<Article> selectAll(String boardid);
	public void update(Article article);
	public void delete(String articleId);
	public void updateSeq(HashMap<String, Integer> hash);
	public Integer maxGrpNo();
	public Integer maxGrpSeq(HashMap<String, Integer> map);
	public List<Article> selectByTitle(HashMap<String, String> hash);
	public int articleCount(String boardId);
	
	
	

}
