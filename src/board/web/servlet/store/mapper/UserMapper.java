package board.web.servlet.store.mapper;

import java.util.HashMap;
import java.util.List;


import board.web.servlet.domain.User;

public interface UserMapper {
	

	public User selectOne(String userId);
	public void create(User user);
	public void update(User user);
	public void delete(String userId);
	public List<User> selectAll();

}
