package board.web.servlet.store.mapper;

import java.util.List;

import board.web.servlet.domain.Board;

public interface BoardMapper {
	public List<Board> selectAll();
	public void createBoard(Board board);
	public Board selectOne(String boardId);
	public void update(Board board);
	public void delete(String boardId);

}
