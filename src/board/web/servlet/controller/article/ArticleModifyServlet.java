package board.web.servlet.controller.article;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.Board;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class ArticleModifyServlet
 */
public class ArticleModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		BoardService service = new BoardServiceLogic();
		Article article = service.findArticle(id);
		Board board = service.findBoard(article.getBoardId(),0);
		List<Board> boards = service.findAllBoards();
		
		request.setAttribute("boardList",boards);
		request.setAttribute("board",board);
		request.setAttribute("article", article);
		
		request.getRequestDispatcher("/WEB-INF/views/article/articleModify.jsp").forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		String articleId = request.getParameter("articleId");
		String contents = request.getParameter("contents");
		
		BoardService service = new BoardServiceLogic();
		
		Article article = service.findArticle(articleId);
		article.setContents(contents);
		
		service.modifyArticle(article);
		response.sendRedirect(request.getContextPath()+"/article/list.do?id="+article.getBoardId());
	}

}
