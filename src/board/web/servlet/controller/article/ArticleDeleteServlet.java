package board.web.servlet.controller.article;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.User;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class ArticleDeleteServlet
 */
public class ArticleDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		
		
		BoardService service = new BoardServiceLogic();
		Article article = service.findArticle(id);
		if(user!=null && user.getUserId().equals(article.getAuthorName())) {
			service.removeArticle(id);
			response.sendRedirect(request.getContextPath()+"/article/list.do?id="+article.getBoardId());
		}
		else {
			request.setAttribute("error", "삭제 권한이 없습니다");
			request.setAttribute("errorTitle", "권한 오류");
			request.getRequestDispatcher("/WEB-INF/views/common/errorPage.jsp").forward(request, response);
		}
		
		
	}

}
