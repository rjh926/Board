package board.web.servlet.controller.article;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.Board;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;


public class ArticleReplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BoardService service = new BoardServiceLogic();
		String articleId = request.getParameter("articleId");
		String boardId = request.getParameter("boardId");
		List<Board> boards = service.findAllBoards();
		Board board = service.findBoard(boardId,0);
		Article article = service.findArticle(articleId);
		
		request.setAttribute("boardList", boards);
		request.setAttribute("board",board);
		request.setAttribute("article", article);
		
		request.getRequestDispatcher("/WEB-INF/views/article/articleWrite.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		BoardService service = new BoardServiceLogic();
		Article parent = service.findArticle(request.getParameter("articleId"));
		String title = request.getParameter("title");
		String authorName = request.getParameter("user");
		String contents = request.getParameter("contents");
		String boardId = request.getParameter("boardId");
		
		Article article = new Article();
		article.setDepth(parent.getDepth()+1);
		article.setGrpNo(parent.getGrpNo());
		article.setTitle(title);
		article.setAuthorName(authorName);
		article.setContents(contents);
		article.setBoardId(boardId);
		article.setGrpSeq(parent.getGrpSeq());
		article.setParentId(parent.getArticleId());
		service.registerArticle(article);
		
		response.sendRedirect(request.getContextPath()+"/article/list.do?id="+boardId);
		
		
		
	}

}
