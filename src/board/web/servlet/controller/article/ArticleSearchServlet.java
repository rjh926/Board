package board.web.servlet.controller.article;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class ArticleSearchServlet
 */
public class ArticleSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("html/text");
		response.setCharacterEncoding("UTF-8");
		String title = request.getParameter("title");
		String boardId = request.getParameter("boardId");
		BoardService service = new BoardServiceLogic();
		JSONObject obj = service.selectByTitle(title,boardId);
		PrintWriter out = response.getWriter();
		String str = obj.toJSONString();
		out.print(str);
	}

}
