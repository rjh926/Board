package board.web.servlet.controller.article;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.Board;
import board.web.servlet.domain.Paging;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class ArticleListServlet
 */
public class ArticleListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		int pageNum = Integer.parseInt(request.getParameter("pageNum"));
		BoardService service = new BoardServiceLogic();

		Board board = service.findBoard(id,pageNum);

		List<Board> boards = service.findAllBoards();
		List<Article> articles = board.getArticles();
		
		request.setAttribute("board", board);
		request.setAttribute("boardList", boards);

		request.getRequestDispatcher("/WEB-INF/views/article/articleList.jsp").forward(request, response);

	}

}
