package board.web.servlet.controller.article;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.web.servlet.domain.Article;
import board.web.servlet.domain.Board;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;


public class ArticleDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("articleId");
		String boardId = request.getParameter("boardId");
		BoardService service = new BoardServiceLogic();
		
		Article article = service.findArticle(id);
		
		if (article != null && "N".equals(article.getIsDelete())) {
			int count = article.getCount();
			article.setCount(++count);
			service.modifyArticle(article);
		}
		
		List<Board> boards = service.findAllBoards();
		Board board = service.findBoard(boardId,0);
		
		
		request.setAttribute("boardList", boards);
		request.setAttribute("article", article);
		request.setAttribute("board", board);
		
		request.getRequestDispatcher("/WEB-INF/views/article/articleDetail.jsp").forward(request, response);
		
		
	}

}
