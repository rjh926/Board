package board.web.servlet.controller.board;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.web.servlet.domain.Board;
import board.web.servlet.domain.User;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class BoardModifyServlet
 */
@WebServlet("/board/modify.do")
public class BoardModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		BoardService service = new BoardServiceLogic();
		List<Board> boards = service.findAllBoards();
		Board board = service.findBoard(request.getParameter("id"),0);
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if(user == null || !user.getAuthority().equals("admin")) {
			request.setAttribute("error","권한이없습니다.");
			request.setAttribute("errorTitle","접근오류");
			request.getRequestDispatcher("/WEB-INF/views/common/errorPage.jsp").forward(request, response);
		}
		else {
		request.setAttribute("boardList",boards);
		request.setAttribute("board",board);
		
		request.getRequestDispatcher("/WEB-INF/views/board/boardModify.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		BoardService service = new BoardServiceLogic();
		String id = request.getParameter("boardId");
		String name = request.getParameter("name");
		
		Board board = new Board();
		board.setBoardId(id);
		board.setName(name);
		service.modifyBoard(board);
		response.sendRedirect(request.getContextPath()+"/WEB-INF/board/list.do");
	}

}
