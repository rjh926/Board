package board.web.servlet.controller.board;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class BoardDeleteServlet
 */
@WebServlet("/board/delete.do")
public class BoardDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String id = request.getParameter("id");
		BoardService service = new BoardServiceLogic();
		
		service.removeBoard(id);
		
		response.sendRedirect(request.getContextPath()+"/board/list.do");
	}

}
