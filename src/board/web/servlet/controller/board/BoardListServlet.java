package board.web.servlet.controller.board;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.web.servlet.domain.Board;
import board.web.servlet.domain.User;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.logic.BoardServiceLogic;

/**
 * Servlet implementation class BoardListServlet
 */
@WebServlet("/board/list.do")
public class BoardListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BoardService service = new BoardServiceLogic();
		
		List<Board> boardList = service.findAllBoards();
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if(user == null || !user.getAuthority().equals("admin")) {
			request.setAttribute("error","권한이없습니다.");
			request.setAttribute("errorTitle","접근오류");
			request.getRequestDispatcher("/WEB-INF/views/common/errorPage.jsp").forward(request, response);
		}
		else {
		request.setAttribute("boardList", boardList);
		
		request.getRequestDispatcher("/WEB-INF/views/board/boardList.jsp").forward(request, response);
	
		}
		}

}
