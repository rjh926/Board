package board.web.servlet.controller.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import board.web.servlet.domain.User;
import board.web.servlet.service.BoardService;
import board.web.servlet.service.UserService;
import board.web.servlet.service.logic.BoardServiceLogic;
import board.web.servlet.service.logic.UserServiceLogic;


@WebServlet("/common/login.do")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		
		String id = request.getParameter("inputEmail");
		String pw = request.getParameter("inputPassword");
		BoardService service = new BoardServiceLogic();
		UserService u = new UserServiceLogic();
		PrintWriter out = response.getWriter();
		int login = u.login(id, pw);
		
		switch (login) {
		case 0:
			User user = u.read(id);
			HttpSession session = request.getSession();
			session.setAttribute("user",user);
			response.sendRedirect(request.getContextPath()+"/board/list.do");
			break;
		case 1:
			user = u.read(id);
			session = request.getSession();
			session.setAttribute("user",user);
			response.sendRedirect(request.getContextPath()+"/article/main.do");break;
		case 2:
			request.setAttribute("error","아이디가 틀렸습니다");
			request.setAttribute("errorTitle","로그인실패");
			request.getRequestDispatcher("/WEB-INF/views/common/errorPage.jsp").forward(request, response);break;
		default:
			request.setAttribute("error","비밀번호가 틀렸습니다");
			request.setAttribute("errorTitle","로그인실패");
			request.getRequestDispatcher("/WEB-INF/views/common/errorPage.jsp").forward(request, response);
			break;
		}
		
		
		
	}

}
