package board.web.servlet.controller.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.web.servlet.domain.User;
import board.web.servlet.service.UserService;
import board.web.servlet.service.logic.UserServiceLogic;


public class UserJoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		request.getRequestDispatcher("/WEB-INF/views/common/join.jsp").forward(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		
		String id = request.getParameter("loginId");
		String pw = request.getParameter("password");
		String name = request.getParameter("name");

		UserService service = new UserServiceLogic();
		User user = new User();
		user.setUserId(id);
		user.setPassword(pw);
		user.setName(name);
		
		service.join(user);
		response.sendRedirect(request.getContextPath());
		
	}

}
