package board.web.servlet.controller.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import board.web.servlet.domain.User;
import board.web.servlet.service.UserService;
import board.web.servlet.service.logic.UserServiceLogic;

/**
 * Servlet implementation class IdCheckServlet
*/
public class IdCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		
		UserService service = new UserServiceLogic();
		PrintWriter out = response.getWriter();
		String id = request.getParameter("id");
		String exist = service.checkId(id);
		
		
		
		if(id.equals(exist)) {
			out.print("false");
		}
		else {
			out.print("ok");
		}
	}

}
